var mongoose = require('mongoose')
var Schema = mongoose.Schema

const tokenSchema = new Schema({
    access_token: {
      type: String
    },
    expiresIn: {
      type: Number
    }
})

tokenSchema.statics.protectedFields = [
    '_id',
    '__v'
]

const Token = mongoose.model('Token', tokenSchema)

module.exports = Token
