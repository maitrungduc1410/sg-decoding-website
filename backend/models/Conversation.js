var mongoose = require('mongoose')
var Schema = mongoose.Schema

const conversationSchema = new Schema({
    name: {
        type: String,
        default: 'Note'
    },
    path: {
      type: String,
      default: ''
    },
    duration: {
        type: Number,
        default: 0
    },
    fileName: {
      type: String
    },
    originalFileName: {
      type: String
    },
    storageName: {
      type: String
    },
    status: {
      type: Array
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    isDone: {
      type: Boolean,
      default: false
    },
    files: {
      type: Array,
      default: []
    }
}, { timestamps: { createdAt: 'created_at' } })

conversationSchema.statics.protectedFields = [
    '_id',
    '__v'
]

const Conversation = mongoose.model('Conversation', conversationSchema)

module.exports = Conversation
