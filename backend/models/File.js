var mongoose = require('mongoose')
var Schema = mongoose.Schema

const fileSchema = new Schema({
    path: {
      type: String,
      default: ''
    },
    fileName: {
      type: String
    },
    originalFileName: {
      type: String
    },
    storageName: {
      type: String
    },
    status: {
      type: Array
    },
    access_token: {
      type: String,
      ref: 'Token'
    },
    isDone: {
      type: Boolean,
      default: false
    },
    files: {
      type: Array,
      default: []
    }
}, { timestamps: { createdAt: 'created_at' } })

fileSchema.statics.protectedFields = [
    '_id',
    '__v'
]

const File = mongoose.model('File', fileSchema)

module.exports = File
