var bcrypt = require('bcryptjs')
var mongoose = require('mongoose')
var Schema = mongoose.Schema

const userSchema = new Schema({
    username: {
      type: String,
      minlength: [6, 'Username must be at least 6 characters long'],
    },
    email: {
      type: String,
      validate: {
          validator: (value) => {
              return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)
          },
          message: '{VALUE} is not a valid email address!'
      },
      minlength: [6, 'Email must be at least 6 characters long'],
    },
    password: { 
      type: String,
      minlength: [6, 'Password must be at least 6 characters long'],
      select: false
    },
    role: {
      type: String,
      enum: ['admin', 'user'],
      default: 'user'
    },
    token: {
      type: String,
      default: '',
      select: false
    }
})

userSchema.post('validate', function (doc, next) {
    if (this.isModified('password')) {
        doc.password = bcrypt.hashSync(doc.password, 10)
    }

    return next()
})

userSchema.statics.protectedFields = [
    '_id',
    '__v',
    'password',
    'role'
]

//authenticate input against database
userSchema.statics.authenticate = function(email, password, callback) {
  User.findOne({
    email: email
  }).select("+password")
  .exec(function(err, user) {
    if (err) {
      return callback(err)
    } else if (!user) {
      var err = new Error('User not found.');
      err.status = 401;
      return callback(err);
    }
    bcrypt.compare(password, user.password, function(err, result) {
      if (result === true) {
        return callback(null, user);
      } else {
        return callback();
      }
    })
  });
}

userSchema.statics.seed = async function () {
    try {
        const users = await this.find()

        if (!users.length) {
          let admin = new this({
            email: 'admin@ntu.edu.sg',
            username: 'admin',
            password: '123456',
            role: 'admin'
          })
          await admin.save()

          let user1 = new this({
            email: 'user1@ntu.edu.sg',
            username: 'user1',
            password: '123456',
            role: 'user'
          })
          await user1.save()

          let user2 = new this({
            email: 'user2@ntu.edu.sg',
            username: 'user2',
            password: '123456',
            role: 'user'
          })
          await user2.save()

          let test1 = new this({
            email: 'test1@ntu.edu.sg',
            username: 'test1',
            password: '123456',
            role: 'user'
          })
          await test1.save()

          let test2 = new this({
            email: 'test2@ntu.edu.sg',
            username: 'test2',
            password: '123456',
            role: 'user'
          })
          await test2.save()

          let test3 = new this({
            email: 'test3@ntu.edu.sg',
            username: 'test3',
            password: '123456',
            role: 'user'
          })
          await test3.save()
        }
    } catch (e) {
        console.log(e)
    }
}
const User = mongoose.model('User', userSchema)

User.seed()

module.exports = User
