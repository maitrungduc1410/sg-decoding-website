var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var env = require('./env');
var fs = require('fs');

// mongoose.connect('mongodb://db:27017/sg-decoding', { useNewUrlParser: true })
// var mongoUrl = 'mongodb://db:27017/sg-decoding'
let mongoUrl = ''

if (process.env.NODE_ENV === 'production') {
  mongoUrl = `mongodb://${env.mongoProd.username}:${env.mongoProd.password}@${env.mongoProd.host}:${env.mongoProd.port}/${env.mongoProd.database}`
} else {
  mongoUrl = env.mongoUrlDev
}

var connectWithRetry = function() { // when using with docker, at the time we up containers. Mongodb take few seconds to finish starting, during that time NodeJS server also tries to start it self, and it may cause the error on trying to connect to MongoDB.
  return mongoose.connect(mongoUrl, { useNewUrlParser: true, useFindAndModify: false }, function(err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
      setTimeout(connectWithRetry, 5000);
    }
  });
};
connectWithRetry();

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
// var apiRouter = require('./routes/api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

require('./middleware/protectPublicFiles')(app) // we need to put this line before set set static folder for Express

app.use(require('cors')({ origin: true, credentials: true }))
app.use(logger('combined', {
  stream: fs.createWriteStream('./log/access.log', {flags: 'a'})
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
// app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
});

module.exports = app;
