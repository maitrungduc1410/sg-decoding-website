module.exports = {
  mongoProd: {
    // url: 'mongodb://db:27017/sg-decoding',
    host: 'db', // this MUST matches the mongodb container name
    port: '27017',
    username: 'user',
    password: 'ZfQDwQYN4T6PQARY',
    database: 'sg-decoding'
  }, // for Production
  storage: 'public/audios/uploads',
  output: 'public/audios/outputs',
  mongoUrlDev: 'mongodb://localhost:27017/sg-decoding',
  serverStorage: {
    english: {
      name: 'English',
      admin: 'ntu-upload',
      password: 'Ntuupload12345',
      host: '40.65.174.255',
      port: '50001',
      path: '/export/data/input'
    },
    mandarin: {
      name: 'Mandarin',
      admin: 'ntu-upload',
      password: 'Ntuupload12345',
      host: '40.65.174.255',
      port: '50003',
      path: '/export/data/input'
    },
    'eng-mandarin': {
      name: 'English-Mandarin',
      admin: 'ntu-upload',
      password: 'Ntuupload12345',
      host: '40.65.174.255',
      port: '50004',
      path: '/export/data/input'
    },
    malay: {
      name: 'Malay',
      admin: 'ntu-upload',
      password: 'Ntuupload12345',
      host: '40.65.174.255',
      port: '50005',
      path: '/export/data/input'
    },
    'eng-malay': {
      name: 'English-Malay',
      admin: 'ntu-upload',
      password: 'Ntuupload12345',
      host: '40.65.174.255',
      port: '50006',
      path: '/export/data/input'
    },
  }
}