const User = require('../models/User')
const Conversation = require('../models/Conversation')
const jwt = require('jsonwebtoken')
var VerifyToken = require('../middleware/auth')
var fs = require('fs')

async function checkIfAdmin(token) {
  let user = await User.findOne({token: token}) // if this token is store in db

  if (!VerifyToken.checkTokenValid(token) || !user || user.role !== 'admin') {
    return false
  }

  return true
}

async function validateUser (req, res) {
  try {
    if (!req.body.email || !req.body.email.trim().length) {
      return res.status(422).json({
        message: 'Email field is required!'
      })
    }
    
    if (!req.body.username || !req.body.username.trim().length) {
      return res.status(422).json({
        message: 'Username field is required!'
      })
    }

    if (!req.body.password || !req.body.password.trim().length) {
      return res.status(422).json({
        message: 'Password field is required!'
      })
    }

    let checkUsername = await User.find({username: req.body.username.trim()})

    if (checkUsername.length) {
      return res.status(422).json({
        message: 'Username already exists!'
      })
    }

    let checkEmail = await User.find({email: req.body.email.trim()})

    if (checkEmail.length) {
      return res.status(422).json({
        message: 'Email already exists!'
      })
    }

    if (req.body.role) { // in this case is only for admin adding new user
      if (!req.body.role.trim().length) {
        return res.status(422).json({
          message: 'Role must not be empty!'
        })
      } else {
        let check = await checkIfAdmin(req.headers['x-jwt-token'])
        if (!check) {
          let message = { auth: false, message: 'Unauthorized' }
          return res.status(401).send(message)
        }
      }
    }
  } catch (e) {
    console.log(e)
    return res.status(500).send(e)
  }
}

class UserController {
    static async all (req, res) {
      try {
        let check = await checkIfAdmin(req.headers['x-jwt-token']) // only admin can view all users
        if (!check) {
          let message = { auth: false, message: 'Unauthorized' }
          return res.status(401).send(message)
        }

        const users = await User.find()

        return res.json({
          users
        })
        
      } catch (e) {
        return res.status(500).send(e)
      }
    }

    static async register (req, res) {
        try {
          
          await validateUser(req, res)

          let user = new User({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            role: req.body.role ? req.body.role.trim() : 'user'
          })

          await user.save()

          if (!req.body.role) { // in this case is user register in Register page
            var token = jwt.sign({ id: user._id }, 'supersecret', {
              expiresIn: 86400 // expires in 24 hours
            })

            user = await User.findByIdAndUpdate(user._id, {token: token})

            return res.json({
                success: true,
                user: user,
                auth: true,
                token: token
            })
          } else { // in this case admin is adding new user in Manage Users page (that means we don't need to generate user token)
            var cloneUser = {
              _id: user._id,
              email: user.email,
              username: user.username,
              role: user.role
            }

            return res.json({
                success: true,
                user: cloneUser
            })
          }
        } catch (e) {
          console.log(e)
          return res.status(500).send(e)
        }
    }

    static async getByToken (token) {
      try {
        const user = await User.findOne({token: token})

        if (!user) {
          return {
            message: 'User not found!'
          }
        }

        return user
      } catch (e) {
        return e
      }
    }
    
    static async remove (req, res) {
        try {
          let check = await checkIfAdmin(req.headers['x-jwt-token']) // only admin can remove users
          if (!check) {
            let message = { auth: false, message: 'Unauthorized' }
            return res.status(401).send(message)
          }

          const user = await User.findById(req.params.id)

          if (user.role === 'admin') { // prevent admin from delete other admin accounts or his account
            return res.status(422).json({
              message: 'You can\'t delete admin account by yourself!'
            })
          }

          if (!user) {
            return res.status(422).json({
              message: 'User not found!'
            })
          }

          let conversations = await Conversation.find({user: user._id})
          
          conversations.forEach(conversation => { // remove all audio files uploaded by this user
            if (conversation.path && fs.existsSync(conversation.path)) {
              fs.unlinkSync(conversation.path)
            }
          })

          await Conversation.deleteMany({user: user._id}) // remove all conversations created by this user

          await user.remove() // remove this user
          
          return res.json({
            success: true
          })
        } catch (e) {
          console.log(e)
          return res.status(500).send(e)
        }
    }

    static async login (req, res, next) {
      if (req.body.email && req.body.password) {
        User.authenticate(req.body.email, req.body.password, async (error, user) => {
            if (error || !user) {
                return res.status(422).json({
                  message: 'Wrong email or password.'
                })
            } else {
              var token = jwt.sign({ id: user._id }, 'supersecret', {
                          expiresIn: 86400 // expires in 24 hours
                      })
              user = await User.findByIdAndUpdate(user._id, {token: token}) // we can't use select(+token) here because the that token is the old one
              res.status(200).send({ auth: true, token: token, user: user })
            }
        })
      }
      else {
        return res.status(422).json({
          message: 'Email and password are required.'
        })
      }
    }

    static async logout (req, res, nex) {
      try {
        let user = await User.findByIdAndUpdate(req.body.userId, {token: ''}) // delete current user's token out of DB
        return res.json({
          success: true,
          userId: user._id
        })
      } catch (e) {
        res.status(500).send(e)
      }
    }
}

module.exports = UserController