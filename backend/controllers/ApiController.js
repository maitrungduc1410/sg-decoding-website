var client = require('scp2')
var upload = require('../modules/upload')
var env = require('../env')
var downloadFileRecursive = require('../modules/download')
var fs = require('fs')
var pathLib = require('path')
var Token = require('../models/Token')
const jwt = require('jsonwebtoken')
var File = require('../models/File')
var archiver = require('archiver')

function getFilesInFolder (path) { // get files in a folder
  let files = fs.readdirSync(path)
  let results = []
  files.forEach(file => {
    const stats = fs.statSync(`${path}/${file}`)
    const fileSizeInBytes = stats.size
    const ext = pathLib.extname(`${path}/${file}`)

    results.push({
      filename: file,
      size: fileSizeInBytes,
      type: ext.substr(1)
    })
  })
  return results
}

function getFileNameWithoutExtension (filename) {
  let fileNameWithoutExtension = filename.trim().split('.')
  fileNameWithoutExtension.pop()
  fileNameWithoutExtension = fileNameWithoutExtension.join('')
  return fileNameWithoutExtension
}

function zipDirectory(source, out) { // compress whole content of folder
  const archive = archiver('zip', { zlib: { level: 9 }});
  const stream = fs.createWriteStream(out);

  return new Promise((resolve, reject) => {
    archive
      .directory(source, false)
      .on('error', err => reject(err))
      .pipe(stream)

    stream.on('close', () => resolve(true))
    archive.finalize()
  })
}

class ApiController {
  static async getAccessToken (req, res) {
    let expiresIn = 86400 * 30 // expires in 30 days
    let tokenValue = jwt.sign({ id: Date.now() }, 'supersecret', {
      expiresIn: expiresIn
    })

    let token = new Token({
      access_token: tokenValue,
      expiresIn: expiresIn
    })

    await token.save()

    return res.json(token)
  }

  static async upload (req, res) {
    let storageName = req.params.storageName // get remote server which user select to upload
    let username = req.params.username

    if (!username || !username.trim().length) {
      return res.status(422).send('Username is empty or not valid')
    } else if (!storageName || !storageName.trim().length) {
      return res.status(422).send('Storage name is empty or not valid')
    }

    let originalname = req.params.originalFilename
    // req.file = {}
    // req.file.originalname = originalname
    upload(req, res, (err) => {
      if (err) {
        console.log(JSON.stringify(err))
        res.status(400).send('fail saving image')
      } else {
        storageName = storageName.trim()

        // copy file to server using scp with format -->> client.scp('file.txt', 'admin:password@example.com:port:/home/admin/', function(err) {})
        client.scp(`${env.storage}/${res.req.file.filename}`, `${env.serverStorage[storageName].admin}:${env.serverStorage[storageName].password}@${env.serverStorage[storageName].host}:${env.serverStorage[storageName].port}:${env.serverStorage[storageName].path}`, async (err) => {
          if (err) {
            res.status(400).send(err)
          } else {
            // if success, do some actions and update to DB
            let file = new File({
              path: `${env.storage}/${res.req.file.filename}`,
              fileName: res.req.file.filename,
              originalFileName: originalname,
              storageName: storageName,
              status: [{status: 'CREATED', created_at: Date.now()}],
              access_token: req.headers['x-access-token']
            })

            await file.save()
            return res.json({
              message: 'success',
              file: file
            })
          }
        })
      }
    })
  }

  static async transcribe (req, res) {

  }

  static async getJobByName (req, res) {
    let fileName = req.params.fileName
    let file = await File.findOne({fileName: fileName})
    if (!file) {
      return res.status(404).send({
        message: 'File not found!'
      })
    }

    return res.json(file)
  }

  static async getTranscriptionByJobName (req, res) {
    let fileName = req.params.fileName
    let file = await File.findOne({fileName: fileName})
    if (!file) {
      return res.status(404).send({
        message: 'File not found!'
      })
    }

    if (!file.isDone) {
      return res.status(422).send({
        message: 'File is in progress!'
      })
    }

    let folderName = getFileNameWithoutExtension(fileName)
    let pathToOutput = 'public/audios/outputs/export/data/output'
    let fileName = `archive-${Date.now()}.zip`
    let fileOutput = `${pathToOutput}/${fileName}`
    zipDirectory(`${pathToOutput}/${folderName}`, fileOutput)
    .then(result => {
      return res.json({
        success: true,
        fileUrl: fileOutput,
        fileName: fileName
      })
    }).catch(error => {
      return res.status(500).send(error)
    })
  }

  static async updateStatus(req, res) {
    let fileName = req.body.filename
    let status = req.body.status
    let file = await File.findOne({fileName: fileName})
    if (file) {
      file.status.push({
        status: status,
        created_at: Date.now()
      })

      let files = []
      if (status.trim().toLowerCase() === 'done') { 
        let storage = env.serverStorage[file.storageName]
        let fileNameWithoutExtension = getFileNameWithoutExtension(file.fileName)
        let conn = downloadFileRecursive(storage.host, storage.port, storage.admin, storage.password, `/export/data/output/${fileNameWithoutExtension}`, env.output)
        
        await conn.on('end', () => {
          files = getFilesInFolder(`${env.output}/export/data/output/${fileNameWithoutExtension}`)
          file.isDone = true
          file.files = files

          file.save()

          return res.json({
            success: true
          })
        })
      } else {
        conversation.save()

        return res.json({
          success: true
        })
      }
    } else {
      let message = {
        message: 'File not found!'
      }
      return res.json(message)
    }
  }
}

module.exports = ApiController