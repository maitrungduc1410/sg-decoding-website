const Conversation = require('../models/Conversation')
const User = require('../models/User')
const io = require('../bin/www')
var fs = require('fs')
var pathLib = require('path')
var VerifyToken = require('../middleware/auth')
var env = require('../env')
var downloadFileRecursive = require('../modules/download')
var archiver = require('archiver')
var upload = require('../modules/upload')
const { getAudioDurationInSeconds } = require('get-audio-duration')
var UserController = require('../controllers/UserController')
var client = require('scp2')
var _ = require('lodash')

async function checkIfUserCan(token, conversationUserId) {
  let user = await User.findOne({token: token})

  if (user.role === 'admin') {
    return true
  }

  if (!user || user._id.toString() !== conversationUserId.toString()) { // need to pass to String because ._id get from DB is 'ObjectId' Class
    return false
  }
  
  return true
}

function getFilesInFolder(path) { // get files in a folder
  let files = fs.readdirSync(path)
  let results = []
  files.forEach(file => {
    const stats = fs.statSync(`${path}/${file}`)
    const fileSizeInBytes = stats.size
    const ext = pathLib.extname(`${path}/${file}`)

    results.push({
      filename: file,
      size: fileSizeInBytes,
      type: ext.substr(1)
    })
  })
  return results
}

function zipDirectory(source, out) { // compress whole content of folder
  const archive = archiver('zip', { zlib: { level: 9 }});
  const stream = fs.createWriteStream(out);

  return new Promise((resolve, reject) => {
    archive
      .directory(source, false)
      .on('error', err => reject(err))
      .pipe(stream)

    stream.on('close', () => resolve(true))
    archive.finalize()
  })
}

class ConversationController {
  static async all (req, res) {
    try {
      let token = req.headers['x-jwt-token']
      let user = await User.findOne({token: token})

      if (!VerifyToken.checkTokenValid(token) || !user) {
        let message = { auth: false, message: 'Failed to authenticate token or session expired' }
        return res.status(440).send(message)
      }
      let conversations = []
      if (user.role === 'admin') { // if current user is admin -> take all conversations
        conversations = await Conversation.find().sort('-created_at')
      } else {
        conversations = await Conversation.find({user: user._id}).sort('-created_at') // if current user's role is 'user' -> just take conversations belong to him
      }
      
      return res.json({
        conversations: conversations
      })
    } catch (e) {
      return res.status(500).send(e)
    }
  }

  static async getConversationById (req, res) {
    try {
      const conversation = await Conversation.findById(req.params.id) // get this conversation info in DB
      let check = await checkIfUserCan(req.headers['x-jwt-token'], conversation.user) // check token sent by user and token of user who own this conversation (to make sure it matches)

      if (!check) {
        return res.status(401).json({
          message: 'Unauthorized!'
        })
      }

      return res.json({
        conversation: conversation
      })
    } catch (e) {
      return res.status(500).send(e)
    }
  }

  static async updateConversationName (req, res) {
    try {
      let conversation = await Conversation.findById(req.params.id)
      let name = req.body.name

      let check = await checkIfUserCan(req.headers['x-jwt-token'], conversation.user)

      if (!check) {
        let message = {
          message: 'Unauthorized!'
        }
        return res.status(401).json(message)
      }

      if (!name || !name.trim().length) {
        let message = {
          message: 'Conversation name is required!'
        }
        return res.status(422).json(message)
      }

      conversation.name = name

      await conversation.save()

      return res.json({
        message: 'success'
      })
    } catch (e) {
      return res.status(500).send(e)
    }
  }

  static async create (data) {
    try {
      const conversation = new Conversation(data)
      
      await conversation.save()

      return conversation
    } catch (e) {
      return e
    }
  }

  static async remove(req, res) {
    try {
      
      const conversation = await Conversation.findById(req.params.id)

      let check = await checkIfUserCan(req.headers['x-jwt-token'], conversation.user)

      if (!check) {
        let message = {
          message: 'Unauthorized!'
        }
        return res.status(401).json(message)
      }

      if (!conversation) {
        let message = {
          message: 'Conversation not found!'
        }
        return res.status(422).json(message)
      }

      if (conversation.path && fs.existsSync(conversation.path)) {
        fs.unlinkSync(conversation.path)
      }

      await conversation.remove()

      return res.json({
        success: true
      })
    } catch (e) {
      return res.status(500).send(e)
    }
  }
  
  static async updateStatus(req, res) { // status received from SG-DECODING, format: {filename: 'test.mp3', status: 'DONE'}, method: POST
    let fileName = req.body.filename
    let status = req.body.status
    let username = fileName.substr(0, fileName.indexOf('-')) // get username from filename
    let conversation = await Conversation.findOne({fileName: fileName}) // because each file name created with name contains unique timestamps, so we find in DB conversation with this filename
    let timeCreated = Date.now() // take current timestamp
    if (conversation) { // if find one conversation
      conversation.status.push({ // push status to this conversation
        status: status,
        created_at: timeCreated
      })

      let files = [] // this array contains output file from SG-DECODING
      if (status.trim().toLowerCase() === 'done') { // if status is DONE -> we have output files
        let storage = env.serverStorage[conversation.storageName] // get path of folder to storage output files in this app
        // let fileNameWithoutExtension = conversation.fileName.substring(0, conversation.fileName.length - 4)
        let fileNameWithoutExtension = conversation.fileName.split('.')
        fileNameWithoutExtension.pop()
        fileNameWithoutExtension = fileNameWithoutExtension.join('')
        // we download file from remote server which contains output file, return connection between this app and that server
        let conn = downloadFileRecursive(storage.host, storage.port, storage.admin, storage.password, `/export/data/output/${fileNameWithoutExtension}`, env.output)
        
        // because the process of download files from remote server is asynchronous so we need to carefully handle it
        // on connection end (that mean it finishes downloading files)
        await conn.on('end', () => {
          files = getFilesInFolder(`${env.output}/export/data/output/${fileNameWithoutExtension}`) // because we download all file in every output folder in remote server (no need to care what inside), now when finish downloading it to our app, we browse to see what inside the output
          conversation.isDone = true // set status for conversation
          conversation.files = files // set output files path for conversation

          conversation.save() // save conversation after edit back to DB
          io.io.sockets.to(username).to('admin').emit('status', { // using Socket.IO, fire an event to only user who own this conversation and to admin (Admin can listen to all) -> to make update in realtime on frontend
            fileName: fileName, 
            status: status, 
            created_at: timeCreated, 
            isDone: conversation.isDone,
            files: files
          })

          return res.json({
            success: true
          })
        })
      } else { // if status is NOT Done
        conversation.save() // just save conversation back after we set status for it
        io.io.sockets.to(username).to('admin').emit('status', { // fire event to frontend to update
          fileName: fileName, 
          status: status, 
          created_at: timeCreated, 
          isDone: conversation.isDone,
          files: files
        })

        return res.json({
          success: true
        })
      }
    } else {
      let message = {
        message: 'File not found!'
      }
      return res.json(message)
    }
  }

  static downloadAllFiles (req, res) {
    let folderName = req.body.folderName
    let pathToOutput = 'public/audios/outputs/export/data/output'
    let fileName = `${folderName}.zip`
    let fileOutput = `${pathToOutput}/${fileName}`
    zipDirectory(`${pathToOutput}/${folderName}`, fileOutput)
    .then(result => {
      return res.json({
        success: true,
        fileUrl: fileOutput,
        fileName: fileName
      })
    }).catch(error => {
      return res.status(500).send(error)
    })
  }

  static deleteArchiveFile (req, res) {
    let archiveFile = req.body.archiveFile
    if (fs.existsSync(archiveFile)) {
      fs.unlinkSync(archiveFile)
    }

    return res.json({
      success: true
    })
  }

  static async upload (req, res) {
    let user = await UserController.getByToken(req.headers['x-jwt-token']) // get user by token sent by client in request
    let username = req.params.username // get username in request
    let storageName = req.params.storageName // get remote server which user select to upload
    if (user.username !== username) { // prevent user from using his token to upload as other user's role
      return res.status(401).json({
        message: 'Unauthorized!'
      })
    }

    if (!username || !username.trim().length) {
      return res.status(422).send('Username is empty or not valid')
    } else if (!storageName || !storageName.trim().length) {
      return res.status(422).send('Storage name is empty or not valid')
    }

    // when developing in local in request, we will have field 'req.file.originalname' natively
    // but inside docker container, that isn't supported natively, so we need to do some tricks
    let originalname = req.params.originalFilename
    // req.file = {}
    // req.file.originalname = originalname
    upload(req, res, (err) => {
      if (err) {
        console.log(JSON.stringify(err))
        res.status(400).send('fail saving image')
      } else {
        storageName = storageName.trim()

        // copy file to server using scp with format -->> client.scp('file.txt', 'admin:password@example.com:port:/home/admin/', function(err) {})
        client.scp(`${env.storage}/${res.req.file.filename}`, `${env.serverStorage[storageName].admin}:${env.serverStorage[storageName].password}@${env.serverStorage[storageName].host}:${env.serverStorage[storageName].port}:${env.serverStorage[storageName].path}`, async (err) => {
          if (err) {
            res.status(400).send(err)
          } else {
            // if success, do some actions and update to DB
            let conversationName = req.params.conversationName
            let duration = await getAudioDurationInSeconds(`${env.storage}/${res.req.file.filename}`)
            let conversation = await ConversationController.create({
              name: !conversationName || !conversationName.trim().length ? 'Note' : conversationName,
              path: `${env.storage}/${res.req.file.filename}`,
              duration: duration,
              fileName: res.req.file.filename,
              originalFileName: originalname,
              storageName: storageName,
              status: [{status: 'CREATED', created_at: Date.now()}],
              user: user._id
            })
            return res.json({
              message: 'success',
              conversation: conversation
            })
          }
        })
      }
    })
  }

  static loadServerStorages (req, res) {
    let storages = _.cloneDeep(env.serverStorage) // clone deep through object
    Object.keys(storages).forEach(key => {
      Object.keys(storages[key]).forEach(attr => {
        if (!['name', 'admin'].includes(attr)) { // we just keep name and admin field before send data to client
          delete storages[key][attr]
        }
      })  
    })
    return res.json({
      storages: storages
    })
  }
}

module.exports = ConversationController