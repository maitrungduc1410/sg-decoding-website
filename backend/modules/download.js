var tar = require('tar-fs');
var zlib = require('zlib');
var SSH = require('ssh2');

/**
 * Transfers an entire directory locally by compressing, downloading and extracting it locally.
 * 
 * @param {SSH} conn A ssh connection of the ssh2 library
 * @param {String} remotePath 
 * @param {String} localPath 
 * @param {Integer|Boolean} compression 
 * @param {Function} cb Callback executed once the transfer finishes (success or error)
 * @see http://stackoverflow.com/questions/23935283/transfer-entire-directory-using-ssh2-in-nodejs
 */
function transferDirectory(conn, remotePath, localPath, compression, cb) {
  var cmd = 'tar cf - "' + remotePath + '" 2>/dev/null';

  if (typeof compression === 'function'){
      cb = compression;
  }else if (compression === true){
      compression = 6;
  }

  // Apply compression if desired
  if (typeof compression === 'number' && compression >= 1 && compression <= 9){
      cmd += ' | gzip -' + compression + 'c 2>/dev/null';
  }else{
      compression = undefined;
  }

  conn.exec(cmd, function (err, stream) {
      if (err){
          return cb(err);
      }
          
      var exitErr;

      var tarStream = tar.extract(localPath);

      tarStream.on('finish', function () {
          cb(exitErr);
      });

      stream.on('exit', function (code, signal) {
          
          if (typeof code === 'number' && code !== 0){
              exitErr = new Error('Remote process exited with code ' + code);
          }else if (signal){
              exitErr = new Error('Remote process killed with signal ' + signal);                
          }
              
      }).stderr.resume();

      if (compression){
          stream = stream.pipe(zlib.createGunzip());
      }

      stream.pipe(tarStream);
  });
}


/**
 * 
 * @param {String} host 
 * @param {String} port 
 * @param {String} username 
 * @param {String} password 
 * @param {String} serverPath 
 * @param {String} localPath 
 */
function downloadFileRecursive (host, port, username, password, serverPath, localPath) {
  var conn = new SSH();

  var connectionSettings = {
      // The host URL
      host: host,
      // The port, usually 22
      port: port,
      // Credentials
      username: username,
      password: password
  };

  conn.on('ready', function () {
      // Use the transfer directory 
      transferDirectory(
          // The SSH2 connection
          conn,
          // The remote folder of your unix server that you want to back up
          serverPath,
          // Local path where the files should be saved
          localPath,
          // Define a compression value (true for default 6) with a numerical value
          true,
          // A callback executed once the transference finishes
          function (err) {
              if (err){
                  throw err;
              };

              console.log('Remote directory succesfully downloaded!');

              // Finish the connection
              conn.end();
          }
      );
  }).connect(connectionSettings);

  return conn
}

module.exports = downloadFileRecursive