var multer = require('multer')
var env = require('../env')

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, env.storage)
    },
    filename: (req, file, cb) => {
      if (file.originalname === 'blob') {
        cb(null, req.params.username + '-' + Date.now() + '.wav')
      } else {
        let mimeType = file.mimetype.split('/')[1]
        // if (mimeType === 'wave') {
          // cb(null, req.params.username + '-' + Date.now() + '.wav')
        // } else {
          cb(null, req.params.username + '-' + Date.now() + '.' + mimeType)
        // }
      }
    }
});
var upload = multer({storage: storage}).single('file');

module.exports = upload