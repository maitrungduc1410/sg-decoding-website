var express = require('express');
var router = express.Router();
var UserController = require('../controllers/UserController')
var VerifyToken = require('../middleware/auth')
/* GET users listing. */

router.post('/login', UserController.login)

router.post('/register', UserController.register)

router.post('/logout', UserController.logout)

router.get('/', VerifyToken.verifyToken, UserController.all)

router.delete('/:id', VerifyToken.verifyToken, UserController.remove)

module.exports = router;
