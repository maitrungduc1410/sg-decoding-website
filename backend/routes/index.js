var express = require('express')
var router = express.Router()
var VerifyToken = require('../middleware/auth')
var ConversationController = require('../controllers/ConversationController')

/* TEST GET home page. */
router.get('/', VerifyToken.verifyToken, function (req, res, next) {
  res.render('index', { title: 'Express' })
})

router.post('/fileUpload/:username/:storageName/:originalFilename/:conversationName?', VerifyToken.verifyToken, ConversationController.upload)

router.get('/loadServerStorages', VerifyToken.verifyToken, ConversationController.loadServerStorages)

router.post('/status', ConversationController.updateStatus)

router.get('/conversations', VerifyToken.verifyToken, ConversationController.all)

router.get('/conversations/:id', VerifyToken.verifyToken, ConversationController.getConversationById)

router.put('/conversations/:id', VerifyToken.verifyToken, ConversationController.updateConversationName)

router.delete('/conversations/:id', VerifyToken.verifyToken, ConversationController.remove)

router.post('/conversations/downloadAllFiles', VerifyToken.verifyToken, ConversationController.downloadAllFiles)

router.post('/conversations/deleteArchiveFile', VerifyToken.verifyToken, ConversationController.deleteArchiveFile)

module.exports = router
