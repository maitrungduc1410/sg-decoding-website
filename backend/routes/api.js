var express = require('express')
var router = express.Router()
var ApiController = require('../controllers/ApiController')
var verifyAccessToken = require('../middleware/auth').verifyAccessToken

router.get('/token', ApiController.getAccessToken)

router.post('/upload/:username/:storageName', verifyAccessToken, ApiController.upload)

router.post('/transcribe', verifyAccessToken, ApiController.transcribe)

router.get('/transcribe/:name', verifyAccessToken, ApiController.getJobByName)

router.get('/transcription/:name', verifyAccessToken, ApiController.getTranscriptionByJobName)

router.post('/status', ApiController.updateStatus)

module.exports = router