var jwt = require('jsonwebtoken')
var Token = require('../models/Token')

function verifyToken(req, res, next) {
	var token = req.headers['x-jwt-token']
	if (!token) {
    return res.status(403).send({ auth: false, message: 'No token provided.' })
  }
	jwt.verify(token, 'supersecret', function(err, decoded) {
		if (err)
			return res.status(440).send({ auth: false, message: 'Failed to authenticate token.' })
		// if everything good, save to request for use in other routes
		req.userId = decoded.id
		next()
	})
}

async function verifyAccessToken (req, res, next) {
  var token = req.headers['x-access-token']
	if (!token) {
    return res.status(403).send({ auth: false, message: 'No Access Token provided.' })
  }

  let tokenObj = await Token.findOne({access_token: token})
  if (!tokenObj) {
    return res.status(403).send({ auth: false, message: 'Access Token not found.' })
  }

	jwt.verify(token, 'supersecret', function(err, decoded) {
		if (err)
			return res.status(440).send({ auth: false, message: 'Failed to authenticate Access Token.' })
		// if everything good
		next()
	})
}

function checkTokenValid(token) {
  if (!token) {
    return false
  }

  let check
  jwt.verify(token, 'supersecret', function(err, decoded) {
		if (err) {
      check = false
    }
		check = true
  })
  return check
}

module.exports = {
  verifyToken,
  checkTokenValid,
  verifyAccessToken
}