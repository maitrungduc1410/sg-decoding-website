var VerifyToken = require('../middleware/auth')

// Protect public file from being accessed by unauthorized users (DONE)
// Files will only be accessed by user who created it (NOT DONE)
module.exports = function (app) {
  app.get('/audios/*', VerifyToken.verifyToken, function(req, res, next) {
    res.download(req.path)
    next()
  })

  app.get(['/*.css', '/*.js'], VerifyToken.verifyToken, function(req, res, next) {
    next()
  })
}