// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import MainLayout from './components/layouts'
import http from '@/helpers/http'
import swal from 'sweetalert'
import moment from 'moment'
import io from 'socket.io-client'
import Bus from '@/helpers/bus'

window.http = http

Vue.use(Bus)
Vue.use(MainLayout)
Vue.config.productionTip = false
Vue.component('Loading', require('./components/partials/Loading').default)

http.interceptors.response.use(function (response) {
  return response
}, function (error) {
  if (error.response.status === 403) { // 403: no token provided
    swal({
      title: 'Action unauthorized',
      text: 'You haven\'t login yet. You will be redirected to the login page!',
      icon: 'warning',
      buttons: {
        catch: {
          text: 'OK'
        }
      },
      dangerMode: true
    })
      .then((willChange) => {
        localStorage.removeItem('jwt')
        localStorage.removeItem('user')
        window.location.href = '/login'
      })
  } else if (error.response.status === 440) { // 422: session expired
    swal({
      title: 'Session expired',
      text: 'Your session has been expired. You will be redirected to the login page!',
      icon: 'warning',
      buttons: {
        catch: {
          text: 'OK'
        }
      },
      dangerMode: true
    })
      .then((willChange) => {
        localStorage.removeItem('jwt')
        localStorage.removeItem('user')
        window.location.href = '/login'
      })
  } else if (error.response.status === 401) { // Unauthorized
    swal({
      title: 'Unauthorized!',
      text: 'You don\'t have permission to view this information',
      icon: 'warning',
      buttons: {
        catch: {
          text: 'OK'
        }
      },
      dangerMode: true
    })
      .then((willChange) => {
        window.location.href = '/'
      })
  } else if (error.response.status === 500) { // file not found/server error
    window.location.href = '/'
  } else {
    return Promise.reject(error)
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data: {
    user: {},
    serverStorages: {},
    isLoading: false,
    socket: {},
    baseUrl: `${window.location.protocol}//${window.location.hostname}:3000`
  },
  mounted () {
    if (localStorage.getItem('jwt') && localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'))

      this.initSockets()
    }
  },
  methods: {
    setJWTForRequest (user, token) { // set JWT token for http request header
      localStorage.setItem('user', JSON.stringify(user))
      localStorage.setItem('jwt', token)
      this.user = user

      http.defaults.headers.common['x-jwt-token'] = localStorage.getItem('jwt')

      this.initSockets()
    },
    async logout () {
      let response = await http.post('/users/logout', {userId: this.user._id})
      if (response.data.success) {
        localStorage.removeItem('jwt')
        localStorage.removeItem('user')
        window.location.href = '/login'
      }
    },
    async loadServerStorages () {
      let response = await http.get('/loadServerStorages')
      this.serverStorages = response.data.storages
    },
    toHHMMSS (secs) {
      var secNum = parseInt(secs, 10)
      var hours = Math.floor(secNum / 3600) % 24
      var minutes = Math.floor(secNum / 60) % 60
      var seconds = secNum % 60
      return [hours, minutes, seconds]
        .map(v => v < 10 ? '0' + v : v)
        .filter((v, i) => v !== '00' || i > 0)
        .join(':')
    },
    normalIOSDate (date) {
      return moment(date).format('DD-MM-YYYY, HH:mm:ss')
    },
    initSockets () {
      const url = `${window.location.protocol}//${window.location.hostname}:3000/`
      this.socket = io(url, {
        reconnection: true,
        reconnectionDelay: 1000,
        reconnectionDelayMax: 5000,
        reconnectionAttempts: Infinity
      })

      this.socket.on('connect', () => {
        if (this.user.role === 'admin') {
          this.socket.emit('join room', {room: this.user.role})
        } else {
          this.socket.emit('join room', {room: this.user.username})
        }
      })

      this.socket.on('status', data => {
        this.$bus.emit('status', data)
      })
    },
    formatBytes (a, b) {
      if (a === 0) return '0 Bytes'
      let c = 1024
      let d = b || 2
      let e = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
      let f = Math.floor(Math.log(a) / Math.log(c))
      return parseFloat((a / Math.pow(c, f)).toFixed(d)) + ' ' + e[f]
    }
  },
  components: { App },
  template: '<App/>'
})
