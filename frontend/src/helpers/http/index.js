import axios from 'axios'

export default axios.create({
  baseURL: `${window.location.protocol}//${window.location.hostname}:3000`,
  // baseURL: `https://videochat.chat-avenue.com:4004`,
  withCredentials: true
  // headers: {
  //   'Authorization': `Bearer ${localStorage.getItem('token')}`,
  //   'Content-Type': 'application/json'
  // }
})
