import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/pages/auth/Login'
// import Register from '@/components/pages/auth/Register'
import Home from '@/components/pages/home/Home'
import Recording from '@/components/pages/home/Recording'
import ViewConversation from '@/components/pages/home/ViewConversation'
import Profile from '@/components/pages/user/Profile'
import User from '@/components/pages/user/User'
import http from '@/helpers/http'

Vue.use(Router)

let router = new Router({
  linkActiveClass: 'active',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/recording',
      name: 'Recording',
      component: Recording,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/conversation/:id',
      name: 'View Conversation',
      component: ViewConversation,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/users',
      name: 'User',
      component: User,
      meta: {
        requiresAuth: true,
        isAdmin: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        guest: true
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    } else {
      if (!http.defaults.headers.common['x-jwt-token']) { // in case we refresh webpage, we need to set JWT token in http header again
        http.defaults.headers.common['x-jwt-token'] = localStorage.getItem('jwt')
      }
      if (to.matched.some(record => record.meta.isAdmin)) { // we can't access root instance here cause it may cause error when user hit refresh page
        let user = JSON.parse(localStorage.getItem('user'))
        if (!user || !user.role || user.role !== 'admin') {
          next({name: 'Home'})
        } else {
          next()
        }
      } else {
        next()
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem('jwt') == null) {
      next()
    } else {
      next({name: 'Home'})
    }
  } else {
    next()
  }
})

export default router
